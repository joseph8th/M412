function jeLorenz(r, t, y0, params)

clear all
close all

sigma = params.sigma;
b = params.b;
save lorParams.mat sigma b r;

[T,Y] = ode45(@lorentzFun, [t(1), t(end)], y0);

figure(1)
hold on
plot(Y(:,1), Y(:,2))