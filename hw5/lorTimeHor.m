function lorTimeHor(r, t, Y0, params)
% Compare time horizons for two trajectories in Lorenz system
% Joseph Edwards, jee8th@unm.edu
% M412 HW5, UNM Sp12

close all

sig = params.sig;
b = params.b;
save lorParams.mat sig b r;

[T1,Y1] = ode45(@lorenzFun, [t(1), t(end)], Y0(1,:));
[T2,Y2] = ode45(@lorenzFun, [t(1), t(end)], Y0(2,:));

figure(1)

% x-t plots
hold on
plot(T1, Y1(:,1), 'g')
plot(T2, Y2(:,2), 'r')
xlabel('t'); ylabel('x');
title(['r=',num2str(r),', sigma=',num2str(sig),', b=',num2str(b)]);
legend(['(',num2str(Y0(1,1)),', ',num2str(Y0(1,2)),', ',num2str(Y0(1,3)),')'],...
    ['(',num2str(Y0(2,1)),', ',num2str(Y0(2,2)),', ',num2str(Y0(2,3)),')']);
grid on
hold off