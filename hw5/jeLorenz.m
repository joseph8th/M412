function jeLorenz(r, t, y0, params)
% Control function to plot Lorenz eqs in 3-D, x-z, x(t) & y(t)
% Joseph Edwards, jee8th@unm.edu
% M412 HW5, UNM Sp12

close all

sig = params.sig;
b = params.b;
save lorParams.mat sig b r;

[T,Y] = ode45(@lorenzFun, [t(1), t(end)], y0);

figure(1)

% 3-D plot
subplot(2,2,1)
hold on
line([min(Y(:,1) - 1), max(Y(:,1) + 1)], [0,0], [0,0],...
    'LineStyle','--','Color','k')
line([0,0], [min(Y(:,2) - 1), max(Y(:,2) + 1)], [0,0],...
    'LineStyle','--','Color','k')
line([0,0], [0,0], [min(Y(:,3) - 1), max(Y(:,3) + 1)],...
    'LineStyle','--','Color','k')
plot3(Y(:,1), Y(:,2), Y(:,3))
xlabel('x'); ylabel('y'); zlabel('z');
az = -70;
el = 25;
view(az, el);
grid on;
axis([min(Y(:,1) - 1), max(Y(:,1) + 1),...
    min(Y(:,2) - 1), max(Y(:,2) + 1),...
    min(Y(:,3) - 1), max(Y(:,3) + 1)]);
hold off;

% x-z plot
subplot(2,2,2)
hold on
line([min(Y(:,1) - 1), max(Y(:,1) + 1)], [0,0],...
    'LineStyle','--','Color',[.8 .8 .8])
line([0,0], [min(Y(:,3) - 1), max(Y(:,3) + 1)],...
    'LineStyle','--','Color',[.8 .8 .8])
plot(Y(:,1), Y(:,3))
grid on
axis([min(Y(:,1) - 1), max(Y(:,1) + 1),...
    min(Y(:,3) - 1), max(Y(:,3) + 1)])
set(gca,'xtick',[],'ytick',[]);
xlabel('x'); ylabel('z');
hold off

% x-t plot
subplot(2,2,3)
hold on
plot(T, Y(:,1))
set(gca,'xtick',[])
xlabel('t'); ylabel('x');
grid on
hold off

% y-t plot
subplot(2,2,4)
hold on
plot(T, Y(:,2))
set(gca,'xtick',[])
xlabel('t'); ylabel('y');
grid on
hold off

% super labels
[ax,h] = suplabel(['r=',num2str(r),', sigma=',num2str(sig),...
    ', b=',num2str(b),', init Y_0=(',num2str(y0(1)),...
    ', ',num2str(y0(2)),', ',num2str(y0(3)),')',...
    ', T in [',num2str(T(1)),', ',num2str(T(end)),']'], 't');