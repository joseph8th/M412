function myvanderpol(r0, ep, phi0, t)
% Joseph Edwards - M412, Sp12, UNM
% Plot approx. VS exact vanderpol curves

close all

% Get matrix of r's at different t's
r = 2*r0./sqrt(r0^2 + exp(-1*ep*t)*(4 - r0^2));

% Compute approximates
x = r.*cos(t + phi0);
x_ = -1*r.*sin(t + phi0);

% Compute exacts
[T,X] = ode15s(@vanderpol, [t(1), t(end)], [x(1), x_(1)]);

figure(1)
hold on
plot(x,x_,'--r', 'LineWidth',2)
plot(X(:,1),X(:,2),'-b')
title(['Vanderpol param: (\epsilon=',num2str(ep),', r_0=', num2str(r0),...
    ', \phi_0=', num2str(phi0),'); T=[',num2str(t(1)),', ',...
    num2str(t(end)),']'])
legend('Approx.','Actual')