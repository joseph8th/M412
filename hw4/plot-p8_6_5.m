clear all
clc

w = 3;

t = 0:2*pi/100:2*pi;
x = sin(t);
y = sin(w*t);

figure(1)
plot(x,y,'--rs')