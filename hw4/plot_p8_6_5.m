function plot_p8_6_5(w)
% Joseph Edwards - M412, Sp12, UNM
% Plots Lissajous curves for one parameter w 

close all

t = -8*pi:0.01:8*pi;
x = sin(t);
y = sin(w*t);

figure(1)
hold on
plot(x,y)
title(['\omega = ', num2str(w)])
xlabel('sin(t)')
ylabel(['sin(',num2str(w),'t)'])
axis([min(x)-0.2, max(x)+0.2, min(y)-0.2, max(x)+0.2])