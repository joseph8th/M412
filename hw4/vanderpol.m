function dx = vanderpol(t,x)
% Joseph Edwards - M412, Sp12, UNM
% Compute exact vanderpol vals

dx = zeros(2,1);
dx(1) = x(2);
dx(2) = 0.01*(1 - x(1)^2)*x(2) - x(1);